<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MarketService;

class MarketWebController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->middleware('auth')->only([
            'addProductForm', 'storeProduct', 'deleteProduct'
        ]);
        
        $this->marketService = $marketService;
    }

    public function showMarket() {
        $products = $this->marketService->getProductList();

        return view('market', compact('products'));
    }

    public function showProduct(int $id) {
        $product = $this->marketService->getProductById($id);

        return view('product', compact('product'));
    }

    public function addProductForm() {
        return view('addProductForm');
    }

    public function storeProduct(Request $request) {
        $this->marketService->storeProduct($request);

        return redirect()->route('main');
    }

    public function deleteProduct(Request $request) {

        $product = $this->marketService->getProductById($request->id);

        $this->authorize('delete', $product);

        $this->marketService->deleteProduct($request);

        return redirect()->route('main');
    }
}
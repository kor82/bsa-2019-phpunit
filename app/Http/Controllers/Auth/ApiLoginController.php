<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Support\Facades\Hash;

class ApiLoginController extends Controller
{

    public function login(Request $request): JsonResponse
    {
        $request->validate([
            'email' => ['required','string'],
            'password' => ['required','string'],
        ]);	

    	$credentials = $request->only('email', 'password');

        if ($this->attempt($credentials)) {

        	$token = $this->refreshToken($request->email);



            return response()->json([
            	'token' => $token,
                'api guard' => Auth::guard('api')->check(),
                'web guard ' => Auth::guard('web')->check(),
                'user' => Auth::id(),
                'user' => Auth::guard('api')->user(),
            ], 201);

        } else {

        	return response()->json([
        		'message' => 'invalid data',
        		'errors' => [
        			'wrong email or password'
        		]
        	], 400);
        }

    }

    protected function refreshToken(string $email): string
    {
    	$token = $this->generateToken();

    	User::whereEmail($email)
    	              ->first()
    	              ->update(['api_token' => hash('sha256', $token)]); 

    	return $token;
    }

    protected function generateToken(): string
    {
    	return Str::random(60);
    }


    protected function attempt(array $credentials): bool
    {
        $checkPassword;

        $user = User::whereEmail($credentials['email'])->first();

        if ($user) {
            $checkPassword = Hash::check($credentials['password'], $user->password);
        }

        if ($user && $checkPassword) {
            return true;
        }

        return false;
    }
}

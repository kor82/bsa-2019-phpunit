<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MarketService;
use App\Http\Resources\Product as ProductResource;

class MarketApiController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showList() {

        $products = $this->marketService->getProductList();

        return ProductResource::collection($products);
    }

    public function store(Request $request) {

        $request->validate([
            'product_name' => ['required','string', 'max:255'],
            'product_price' => ['required','numeric', 'min:0'],
        ]);

        $product = $this->marketService->storeProduct($request);

        return new ProductResource($product);
    }

    public function showProduct(int $id) {
        
        $product = $this->marketService->getProductById($id);

        return new ProductResource($product);
    }

    public function delete(Request $request) {

        $product = $this->marketService->getProductById($request->id);

        $this->authorize('delete', $product);

        $this->marketService->deleteProduct($request);

        return response()->json(null, 204);
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Auth\ApiLoginController@login');

Route::get('/items', 'MarketApiController@showList');
Route::post('/items', 'MarketApiController@store')->middleware('auth:api');
Route::get('/items/{id}', 'MarketApiController@showProduct');
Route::delete('/items/{id}', 'MarketApiController@delete')->middleware('auth:api');

Route::fallback(function(){
    return response()->json(['error' => 'Resource not found.'], 404);
})->name('fallback');
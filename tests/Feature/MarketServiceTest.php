<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Repositories\ProductRepository;
use App\Entities\Product;

class MarketServiceTest extends TestCase
{

    private $productRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->productRepository = $this->getMockBuilder(ProductRepositoryInterface::class)
                                        ->getMock();

        $this->productRepository->expects($this->any())
                                ->method('findAll')
                                ->will($this->returnCallback([$this, 'getProductCollection']));

        $this->productRepository->expects($this->any())
                                ->method('findById')
                                ->will($this->returnCallback([$this, 'getProductById']));

        $this->productRepository->expects($this->any())
                                ->method('findByUserId')
                                ->will($this->returnCallback([$this, 'getProductByUserId']));
       
        $this->productRepository->expects($this->any())
                                ->method('store')
                                ->will($this->returnCallback([$this, 'storeProduct']));
    }


    public function test_get_product_list()
    {
        $products = $this->productRepository->findAll();

        $this->assertNotEmpty($products);

        $this->assertInstances($products);
    }

    public function test_get_product_by_id()
    {
        $id = rand(1,5);

        $products = $this->getProductCollection();

        $product = $this->productRepository->findById($id, $products);

        $this->assertInstanceOf(Product::class, $product);
    }

    public function test_get_products_by_user_id()
    {
        $userId = rand(1,2);

        $product = $this->productRepository->findByUserId($userId);

        $this->assertInstanceOf(Product::class, $product);
    }

    public function test_store_product()
    {
        $product = new Product([
            'name' => 'TestPhone',
            'price' => 100.45, 
            'user_id' => rand(1,2)
        ]);

        $products = $this->productRepository->store($product);

        $this->assertCount(6, $products);
    }

    public function getProductCollection(): array
    {
        return [
            new Product ([
                'id' => 1,
                'name'      => 'Apple MacBook',
                'price'     => '1999.99',
                'user_id'   => 1
            ]),            
            new Product ([
                'id' => 2,                
                'name'  => 'Apple iPhone',
                'price' => '999.99',
                'user_id'   => 1
            ]),            
            new Product ([
                'id' => 3,                
                'name'  => 'Apple iPad',
                'price' => '499.99',
                'user_id'   => 1
            ]),            
            new Product ([
                'id' => 4,                
                'name'  => 'Apple Watch',
                'price' => '299.99',
                'user_id'   => 2
            ]),            
            new Product ([
                'id' => 5,                
                'name'  => 'Apple Airpods',
                'price' => '99.99',
                'user_id'   => 2
            ]),
        ];
    }

    public function getProductById(int $id, array $products): ?Product
    {
        return collect($products)->where('id', $id)->first();
    }

    public function getProductByUserId(int $userId): ?Product
    {
        return collect($this->getProductCollection())
                                 ->where('user_id', $userId)
                                 ->first();
    }

    public function storeProduct(Product $product): array
    {
        $products = $this->getProductCollection();

        $products[] = $product;

        return $products;
    }


    public function assertInstances(array $products): void
    {
        foreach ($products as $product) {
            $this->assertInstanceOf(Product::class, $product);
        }        
    }
}

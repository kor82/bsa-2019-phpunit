<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Entities\Product;
use Faker\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class MarketApiTest extends TestCase
{


    protected $faker;

    protected function setUp(): void 
    {
        parent::setUp();

        $this->faker = Factory::create();
    }    


    public function test_show_product()
    {
        $product = factory(Product::class)->create();

        $this->assertDatabaseHas('products', [
            'name' => $product->name
        ]);

        $this->json("GET", "/api/items/{$product->id}")
             ->assertJsonStructure([
                    'id',
                    'name',
                    'price',
                    'user_id',
               ])
             ->assertOk();
    }


    public function test_show_product_list()
    {
        factory(Product::class, 10)->create();

        $this->json("GET", "/api/items")
             ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'price',
                    'user_id',
                ]
            ])
            ->assertOk();
    }

    public function test_auth_user_can_create_product()
    {
        $user = factory(User::class)->create();

        $this->login($user);

        $this->assertAuthenticatedAs($user, 'api');

        $data = [
            'product_name' => 'test_product_'.$this->faker->word,
            'product_price' => $this->faker->randomFloat(2, 0.01, 1000),
        ];

        $this->json("POST", "/api/items", $data)
             ->assertStatus(201);

        $this->assertDatabaseHas('products', [
            'name' => $data['product_name'],
            'price' => $data['product_price'],
        ]);
    }


    public function test_non_auth_user_cant_create_product()
    {
        $product = factory(Product::class)->create();

        $data = [
            'product_name' => 'test_product_'.$this->faker->word,
            'product_price' => $this->faker->randomFloat(2, 0.01, 1000),
        ];

        $this->json("POST", "/api/items", $data)
             ->assertUnauthorized();
    }

    public function test_auth_user_can_delete_own_product()
    {
        $user = factory(User::class)->create();

        $product = factory(Product::class)->create(['user_id' => $user->id]);

        $this->login($user);

        $this->assertAuthenticatedAs($user, 'api');

        $this->json("DELETE", "/api/items/{$product->id}")
             ->assertStatus(204);
    }


    public function test_auth_user_cant_delete_other_product()
    {
        $user = factory(User::class)->create();

        $otherProduct = factory(Product::class)->create(['user_id' => 1]);

        $this->login($user);

        $this->assertAuthenticatedAs($user, 'api');

        $this->json("DELETE", "/api/items/{$otherProduct->id}")
             ->assertForbidden();
    }


    protected function login(User $user): void
    {
        Auth::guard('api')->setUser($user);
    }


}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Entities\Product;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {

	$randomUserId = User::orderByRaw('RAND()')->first()->id;

    return [
            'name' => 'test_product_'.$faker->word,
            'price' => $faker->randomFloat(2, 0.01, 1000),
            'user_id' => $randomUserId,
    ];
});
